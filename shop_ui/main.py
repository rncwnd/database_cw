from shop_ui import app
from shop_ui import home, create_category, delete_category, category_report, publisher_report
from shop_ui import order_history, performance_report, apply_discount, reset_db, create_db
from shop_ui import init_db

if __name__ == '__main__':
    app.run(debug=True)  # Disable for prod i guess
