from flask import Flask, render_template, request, redirect
import os
import psycopg2

app = Flask(__name__)
app.secret_key = b'Good-Cool'

def get_conn():
    conn = psycopg2.connect("dbname=DatabaseCoursework user=postgres")
    return conn
