from shop_ui import *
import psycopg2

@app.route('/category_report', methods=['POST'])
def category_report():
    try:
        conn = get_conn()
        cur = conn.cursor()

        # Make the table
        cur.execute('''CREATE TABLE overview (category_name, title_count, avg_price) AS
                     SELECT category.name, COUNT(DISTINCT title) as count, AVG(price) AS avg_price
                     FROM book
                     JOIN category on book.categoryid = category.categoryid
                     GROUP BY category.categoryid;''')
        cur.execute('''INSERT INTO overview (category_name, title_count, avg_price)
                     VALUES
                     (\'Total\', (SELECT SUM(title_count) from overview), (SELECT AVG(avg_price) FROM overview));''')
        conn.commit()
       
        # Fetch the table
        cur.execute('SELECT * FROM overview')
        rows = cur.fetchall()

        if rows:
            return render_template('category_report.html', rows = rows)
        else:
            return render_template('home.html', msg = 'category_report returned no data!')

    except Exception as e:
        print(e)
        return render_template("home.html", error = e)

    finally:
        if conn:
            conn.close()


@app.route('/delete_report', methods=['POST'])
def delete_report():
    try:
        conn = get_conn()
        cur = conn.cursor()
        cur.execute('DROP TABLE overview')
        conn.commit()
        return redirect('/')

    except Exception as e:
        print(e)
        return render_template("home.html", error = e)

    finally:
        if conn:
            conn.close()
