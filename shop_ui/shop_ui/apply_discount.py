from shop_ui import *
import psycopg2

@app.route('/apply_discount', methods=['POST'])
def apply_discount():
    try:
        conn = get_conn()
        cur = conn.cursor()

        categoryID = request.form.get('categoryID')
        discountPercent = float(request.form.get('discountPercent'))

        cur.execute('''UPDATE book \
        SET price = price * %s \
        FROM category \
        WHERE book.categoryid = %s;''', [discountPercent, categoryID])
        conn.commit()

        cur.execute('SELECT * FROM BOOK WHERE book.categoryid = %s', categoryID)
        rows = cur.fetchall()

        if rows:
            return render_template('apply_discount.html', rows = rows)
        else:
            return render_template('home.html', msg = "No Data!")

    except Exception as e:
        print(e)
        return render_template("home.html", error = e)

    finally:
        if conn:
            conn.close()
