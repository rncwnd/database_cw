from shop_ui import *
import psycopg2

@app.route('/publisher_report', methods=['POST'])
def publisher_report():
    try:
        conn = get_conn()
        cur = conn.cursor()

        # Get our post data
        publisherName = request.form['publisherName']

        # Do the query
        cur.execute('''SELECT book.bookid, book.title book_title, book.price,
        publisher.name p_name,
        orderline.quantity order_qty, orderline.unitsellingprice usp,
        SUM(orderline.unitsellingprice * orderline.quantity) AS order_value,
        SUM(book.price * orderline.quantity) AS retail_value,
        shoporder.orderdate
        FROM book
        JOIN publisher ON book.publisherid = publisher.publisherid
        JOIN orderline ON book.bookid = orderline.bookid
        JOIN shoporder ON orderline.shoporderid = shoporder.shoporderid
        WHERE publisher.name = %s
        GROUP BY book.bookid, title, price, name, quantity, unitsellingprice, orderdate
        ORDER BY orderdate''', [publisherName])

        conn.commit()
        rows = cur.fetchall()

        if rows:
            return render_template('publisher_report.html', publisherName = publisherName, rows = rows)
        else:
            return render_template('home.html', msg = 'No Data Returned')

    except Exception as e:
        print(e)
        return render_template("home.html", error = e)

    finally:
        if conn:
            conn.close()
