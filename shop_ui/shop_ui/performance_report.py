from shop_ui import *
import psycopg2

@app.route('/performance_report', methods=['POST'])
def performance_report():
    try:
        conn = get_conn()
        cur = conn.cursor()

        startDate = request.form.get('startDate')
        endDate = request.form.get('endDate')

        cur.execute('''SELECT salesrep.name,
        SUM(CASE WHEN shoporder.orderdate
            BETWEEN %s AND %s
             THEN orderline.quantity * orderline.unitsellingprice
             ELSE 0 END) AS sales_value,
        SUM(CASE WHEN shoporder.orderdate
            BETWEEN %s AND %s
             THEN orderline.quantity
             ELSE 0 END) AS total_units_sold,
        COUNT(CASE WHEN shoporder.orderdate
              BETWEEN %s AND %s
               THEN orderline.quantity
               END) AS sales_count
        FROM salesrep
        JOIN shoporder ON salesrep.salesrepid = shoporder.salesrepid
        JOIN orderline ON shoporder.shoporderid = orderline.shoporderid
        GROUP BY salesrep.name
        ORDER BY sales_value DESC;''',
                    [startDate, endDate, startDate, endDate, startDate, endDate])

        conn.commit()
        rows = cur.fetchall()

        if rows:
            return render_template('performance_report.html', rows = rows)
        else:
            return render_template('home.html', msg = "No Data!")

    except Exception as e:
        print(e)
        return render_template("home.html", error = e)

    finally:
        if conn:
            conn.close()
