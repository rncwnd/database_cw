from shop_ui import *
import psycopg2

@app.route('/delete_category', methods=['POST'])
def delete_category():
    try:
        conn = get_conn()
        cur = conn.cursor()

        # Get our POST
        categoryID = request.form['categoryID']

        # Execute our query
        cur.execute('DELETE FROM category WHERE categoryid = %s;', [categoryID])
        conn.commit()

        cur.execute('SELECT * FROM category')
        rows = cur.fetchall()

    except Exception as e:
        print(e)

    finally:
        if conn:
            conn.close()
        return redirect('/')
