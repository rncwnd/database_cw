from shop_ui import *
import psycopg2

@app.route('/create_category', methods=['POST'])
def create_category():
    try:
        # Get a DB connection and cursor
        conn = get_conn()
        cur = conn.cursor()

        # Get data out of that POST
        categoryID = request.form['categoryID']
        categoryName = request.form['categoryName']
        categoryType = request.form['categoryType']

        cur.execute('''INSERT INTO category(categoryid, name, categorytype)
        VALUES (%s, %s, %s)''',
                    [categoryID, categoryName, categoryType])
        conn.commit()

        rows = cur.fetchall()

    except Exception as e:
        print(e)

    finally:
        if conn:
            conn.close()
        return redirect('/')
