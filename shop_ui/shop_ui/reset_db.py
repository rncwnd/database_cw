from shop_ui import *
import psycopg2

@app.route('/reset_db', methods=['POST'])
def reset_db():
    try:
        conn = get_conn()
        cur = conn.cursor()
        cur.execute('DROP SCHEMA public CASCADE;')
        cur.execute('CREATE SCHEMA public;')
        conn.commit()
        return redirect('/')

    except Exception as e:
        print(e)
        return render_template("home.html", error = e)

    finally:
        if conn:
            conn.close()
