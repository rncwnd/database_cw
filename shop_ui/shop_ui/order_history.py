from shop_ui import *
import psycopg2

@app.route('/order_history', methods=['POST'])
def order_history():
    try:
        conn = get_conn()
        cur = conn.cursor()

        bookID = request.form.get('bookID')

        cur.execute('''CREATE TABLE order_history(title, retail_price, order_date, unit_selling_price, quantity, order_value, shop_name) AS
        SELECT book.title, book.price retail_price,
        shoporder.orderdate,
        orderline.unitsellingprice, orderline.quantity,
        SUM(orderline.unitsellingprice * orderline.quantity) AS order_value,
        shop.name
        FROM orderline
        JOIN shoporder ON orderline.shoporderid = shoporder.shoporderid
        JOIN book ON orderline.bookid = book.bookid
        JOIN shop ON shoporder.shopid = shop.shopid
        WHERE orderline.bookid = %s
        GROUP BY orderdate, unitsellingprice, quantity, title, price, shop.shopid;''', [bookID])

        cur.execute('''INSERT INTO order_history(title, retail_price, order_date, unit_selling_price, quantity, order_value, shop_name)
        VALUES
        (\'Summary\',
        (SELECT SUM(retail_price) FROM order_history),
        current_timestamp,
        (SELECT SUM(unit_selling_price) FROM order_history),
        (SELECT SUM(quantity) FROM order_history),
        (SELECT SUM(order_value) FROM order_history),
        \'Summary\');''')
        conn.commit()

        cur.execute('SELECT * FROM order_history;')
        rows = cur.fetchall()

        if rows:
            return render_template('order_history.html', rows = rows)
        else:
            return render_template('home.html', msg = "order_history returned no data!")

    except Exception as e:
        print(e)
        return render_template("home.html", error = e)

    finally:
        if conn:
            conn.close()

@app.route('/delete_order_report', methods=['POST'])
def delete_order_report():
    try:
        conn = get_conn()
        cur = conn.cursor()
        cur.execute('DROP TABLE order_history')
        conn.commit()
        return redirect('/')

    except Exception as e:
        print(e)
        return render_template("home.html", error = e)

    finally:
        if conn:
            conn.close()
