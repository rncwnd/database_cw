CREATE TABLE Category
(
	CategoryID		INTEGER NOT NULL,
	Name			VARCHAR(50) NOT NULL
								CHECK(Name != '' AND Name != ' '),
	CategoryType 	VARCHAR(20) NOT NULL
								CHECK(CategoryType != '' AND CategoryType != ' '),
	PRIMARY KEY(CategoryID)
);

CREATE TABLE SalesRep
(
	SalesRepID		INTEGER NOT NULL,
	Name			VARCHAR(50) NOT NULL
								CHECK(Name != '' AND Name != ' '),
	PRIMARY KEY(SalesRepID)
);

CREATE TABLE Shop
(
	ShopID			INTEGER NOT NULL,
	Name			VARCHAR(50) NOT NULL
								CHECK(Name != '' AND Name != ' '),
	PRIMARY KEY(ShopID)
);

CREATE TABLE Publisher
(
	PublisherID		INTEGER NOT NULL,
	Name			VARCHAR(50) NOT NULL
								CHECK(Name != '' AND Name != ' '),
	PRIMARY KEY(PublisherID)
);

CREATE TABLE Book
(
	BookID			INTEGER NOT NULL,
	Title			VARCHAR(50) NOT NULL
								CHECK(Title != '' AND Title != ' '),
	Price			DECIMAL(10,2) NOT NULL
								  CHECK(Price > 0.01),
	CategoryID		INTEGER NOT NULL,
	PublisherID		INTEGER NOT NULL,
	PRIMARY KEY(BookID),
	FOREIGN KEY(CategoryID) REFERENCES Category(CategoryID) ON DELETE RESTRICT,
	FOREIGN KEY(PublisherID) REFERENCES Publisher(PublisherID) ON DELETE RESTRICT
);

CREATE TABLE ShopOrder
(
	ShopOrderID		INTEGER NOT NULL,
	OrderDate		DATE NOT NULL,
	ShopID			INTEGER NOT NULL,
	SalesRepID		INTEGER NOT NULL,
	PRIMARY KEY(ShopOrderID),
	FOREIGN KEY(ShopID) REFERENCES Shop(ShopID) ON DELETE RESTRICT,
	FOREIGN KEY(SalesRepID) REFERENCES SalesRep(SalesRepID) ON DELETE RESTRICT
);

CREATE TABLE Orderline
(
	ShopOrderID		 INTEGER NOT NULL,
	BookID			 INTEGER NOT NULL,
	Quantity		 INTEGER NOT NULL
					 		 CHECK(Quantity < 10),
	UnitSellingPrice DECIMAL (10,2) NOT NULL
					 		 CHECK (UnitSellingPrice > 0),
	FOREIGN KEY(ShopOrderID) REFERENCES ShopOrder(ShopOrderID) ON DELETE CASCADE,
	FOREIGN KEY(BookID) REFERENCES Book(BookID) ON DELETE CASCADE
);
