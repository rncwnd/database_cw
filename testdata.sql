INSERT INTO public.category(categoryid,name,categorytype)
VALUES
    (1, 'Biology', 'Non-Fiction'),
    (2, 'Physics', 'Non-Fiction'),
    (3, 'Computer Science', 'Non-Fiction'),
    (4, 'Manga', 'Fiction'),
    (5, 'SciFi', 'Fiction'),
    (6, 'Government', 'Non-Fiction');
CREATE INDEX category_index ON category(categoryid);

INSERT INTO public.publisher(publisherid, name)
VALUES
    (1, 'Pan Books'),
    (2, 'Kodansha'),
    (3, 'Tokuma Shoten'),
    (4, 'Prentice Hall'),
    (5, 'Bantam Books'),
    (6, 'John Murray'),
    (7, 'Harvill Secker');
CREATE INDEX publisher_index ON publisher(publisherid);

INSERT INTO public.book(bookid, title, price, categoryid, publisherid)
VALUES
    (1, 'The Hitchhikers Guide To The Galaxy', 4.2,
          (SELECT categoryid FROM category WHERE name = 'SciFi'), 1),
    (2, 'Initial D Volume 1', 9.60,
              (SELECT categoryid FROM category WHERE name = 'Manga'), 2),
    (3, 'Trigun Volume 1', 9.0,
             (SELECT categoryid FROM category WHERE name = 'Manga'), 3),
    (4, 'The C Programming Language, 2nd Ed', 25.0,
          (SELECT categoryid FROM category WHERE name = 'Computer Science'), 4),
    (5, 'On The Origin Of Species', 19.99,
         (SELECT categoryid FROM category WHERE name = 'Biology'), 5),
    (6, 'A Brief History Of Time', 15.0,
        (SELECT categoryid FROM category WHERE name = 'Physics'), 6),
    (7, '1984', 4.51,
             (SELECT categoryid FROM category WHERE name = 'Government'),
             (SELECT publisherid FROM publisher WHERE name = 'Harvill Secker')),
    (8, 'Akira', 9.00,
             (SELECT categoryid FROM category WHERE name = 'Manga'),
             (SELECT publisherid FROM publisher WHERE name = 'Kodansha'));
CREATE INDEX book_index ON book(bookid);

INSERT INTO public.salesrep(salesrepid, name)
VALUES
    (1, 'John Doe'),
    (2, 'Jane Doe'),
    (3, 'Sam Smith'),
    (4, 'Hastur');
CREATE INDEX salesrep_index ON salesrep(salesrepid);

INSERT INTO public.shop(shopid, name)
VALUES
       (1, 'NotAFront'),
       (2, 'BlockBuster Somehow'),
       (3, 'Amazonia');
CREATE INDEX shop_index ON shop(shopid);

INSERT INTO public.shoporder(shoporderid, orderdate, shopid, salesrepid)
VALUES
    (1, '2019-01-09', 1, (SELECT salesrepid FROM salesrep WHERE name = 'John Doe')),
    (2, '2019-02-08', 2, (SELECT salesrepid FROM salesrep WHERE name = 'Jane Doe')),
    (3, '2019-03-08', 2, (SELECT salesrepid FROM salesrep WHERE name = 'Jane Doe')),
    (4, '2018-12-07', 3, (SELECT salesrepid FROM salesrep WHERE name = 'Sam Smith')),
    (5, '2009-01-02', 3, (SELECT salesrepid FROM salesrep WHERE name = 'Hastur')),
    (6, '2019-04-09', 2, (SELECT salesrepid FROM salesrep WHERE name = 'Jane Doe')),
    (7, '2009-01-02', 3, (SELECT salesrepid FROM salesrep WHERE name = 'Hastur')),
    (8, '2009-01-02', 3, (SELECT salesrepid FROM salesrep WHERE name = 'Hastur')),
    (9, '2009-01-03', 3, (SELECT salesrepid FROM salesrep WHERE name = 'Hastur')),
    (10, '2009-01-04', 3, (SELECT salesrepid FROM salesrep WHERE name = 'Hastur'));
CREATE INDEX shoporder_index ON shoporder(shoporderid);

INSERT INTO public.orderline(shoporderid, bookid, quantity, unitsellingprice)
VALUES
    (1, 1, 2, 3.0),
    (2, 2, 1, 8.0),
    (3, 3, 1, 8.0),
    (4, 4, 2, 20.0),
    (5, 5, 3, 15.0),
    (6, 6, 5, 10.0),
    (7, 7, 2, 3.0),
    (8, 8, 4, 7.0),
    (9, 2, 3, 8.0),
    (10, 2, 2, 8.0);
CREATE INDEX orderline_index ON orderline(shoporderid,bookid);
